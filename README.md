# Lenguajes de programación y algoritmos
## 1. Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
    Fontcolor white
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Programando ordenadores en\nlos 80 y ahora. ¿Qué ha pasado? <<crimson>>
 * Sistemas antiguos <<green>>
  *_ sus
   * características
    *_ eran
     * ordenadores de los\n80's y 90's <<rose>>
      *_ los recursos
       * del hardware\neran muy limitados <<lime>>
        *_ las compañias
         * hacian\nequipos distintos <<blue>>
  *_ la
   * programación
    *_ el programador
     * conocía la\narquitectura del\ncomputador <<rose>>
      *_ los recursos
       * se aprovechaban bien.\nlos programas no eran\nmultiplataforma <<lime>>
        *_ se programaba
         * directo\nsobre el hardware <<blue>>
          *_ no habia
           * bugs en las\nactualizaciones <<orange>>
  *_ los
   * lenguajes de\nprogramación
    *_ solo se programaba
     * en ensamblador para\nuna mejor eficiencia <<rose>>
      *_ el codigo
       * no era el mismo para\ntodos los\nordenadores <<salmon>>
 * Sistemas actuales <<green>>
  *_ sus
   * características <<gold>>
    *_ los ordenadores actuales
     * tienen una gran\npotencia y\nmemoria" <<rose>>
      *_ los ordenadores
       * son faciles de\nadquirir
        *_ los ordenadores
         * tratan de superar a la\nanterior en rendimiento <<blue>>
  *_ la
   * programación <<gold>>
    *_ el programador
     * no siempre sabe\ncomo trabaja el\nhardware <<rose>>
      *_ los programas
       * son multiplataforma
        *_ hay sobrecarga de capas
         * en la programación\ndebido a la\nabstracción <<blue>>
          *_ los programas
           * son ineficientes en cuestion\nde tiempo de procesamiento <<orange>>
  *_ los
   * lenguajes de\nprogramación <<gold>>
    *_ lenguajes
     * actuales son\nmuy entendibles <<rose>>
      *_ los compiladores
       * son muy importantes para\nejecutar programas en\ndiferentes ordenadores
        *_ hay
         * variedad de lenguajes <<blue>>
@endmindmap
```
## 2. Hª de los algoritmos y de los lenguajes de programación (2010)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
    Fontcolor white
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Historia de los algoritmos y de\nlos lenguajes de programación <<orange>>
 *_ historia
  * han apoyado en el\ndesarrollo de las\nmatemáticas <<cyan>>
   *_ por ejemplo
    * en las calculadoras <<green>>
 *_ algoritmo
  * secuencia ordenada y\nfinita de instrucciones\nque resuelven un problema <<cyan>>
   *_ cuenta con
    * un inicio y un final <<green>>
   *_ siglo XIX
    * surgen las primeras\nmáquinas programables <<green>>
     *_ siglo XX
      * nacen los primeros\nordenadores <<gold>>
       *_ por ende
        * surgen los primeros\nprogramas <<blue>>
 *_ programa
  * es un conjunto de operaciones\nque realiza la computadora <<lime>>
 *_ lenguajes
  * para programar los programas\nsurgen los lenguajes de\nprogramación <<lime>>
  *_ son
   * instrumentos para\ncomunicar los algoritmos\na las maquinas <<lime>>
    *_ por ejemplo
     * Fortran que surge\nen 1960 <<green>>
      *_ es para
       * simplificar funciones <<rose>>
     * Lisp surge en 1968 <<green>>
      *_ compuesto por
       * serie de cédulas\ncomunicadas\npor mensajes <<rose>>
     * Simula surge\nen 1971 <<green>>
      * programación lógica <<rose>>
@endmindmap
```
## 3. Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .salmon {
    BackgroundColor LightSalmon
  }
  .lime {
    BackgroundColor Lime
  }
  .orchid {
    BackgroundColor Orchid
  }
  .crimson {
    BackgroundColor Crimson
  }
  .gold {
    BackgroundColor Gold
  }
  .orange {
    BackgroundColor Orange
  }
  .cyan {
    BackgroundColor lightcyan
  }
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .blue {
    BackgroundColor lightblue
  }
}
</style>
* Lenguajes y paradigamas\nde programación <<gold>>
 * Lenguajes de programación <<rose>>
  *_ de
   * alto nivel <<green>>
    *_ ejemplos
     * Java\nC++\nC\nPython <<orange>>
     * Rubí\nSwitf\nKotlin <<orange>>
  *_ de
   * bajo nivel <<green>>
    *_ ejemplos
     * Ensamblador <<orange>>
     * Código maquina <<orange>>
 * Paradigmas de programación <<rose>>
  *_ se refiere
   * a las formas de\nenfrentar un problema <<green>>
    *_ ejemplos
     * programación orientada\na aspectos <<blue>>
      *_ busca
       * apsectos diferenciados\npara después integrarlos\npara un funcionamiento\ndeterminado <<orchid>>
     * programación orientada\na agentes <<blue>>
      *_ son
       * aplicaciones informáticas <<orchid>>
        *_ forman
         * también sistemas\nmultiagente <<lime>>
        *_ son capaces
         * de decidir automáticamente y\nnegociar con otro agentes <<lime>>
     * programación\nestructurada <<blue>>
      *_ no depende de
       * la arquitectura del hardware <<orchid>>
     * programación\nfuncional <<blue>>
      *_ se fundamenta en
       * matemáticas a partir de\nuna necesidad de\nverificación <<orchid>>
     * programación orientada\na objetos <<blue>>
      *_ ve al mundo como
       * objetos <<salmon>>
     * programación\nlógica <<blue>>
      *_ utiliza
       * expresiones lógicas basándose\nen un lenguaje lógico <<salmon>>
     * programación\ndistribuida <<blue>>
      *_ utiliza
       * redes de ordenadores <<salmon>>
     * programación orientada\na componentes <<blue>>
      *_ usa
       * un nivel mayor de\nabstracción <<salmon>>
        *_ los cuales son
         * un conjunto de objetos
@endmindmap
```